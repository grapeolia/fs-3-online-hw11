package HW11;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

public class Child extends Human{
    private String childSex = "unknown";
    public Child() {
    }

    public Child(String name, String surname, long birthDate, Sexes childSex) {
        super(name, surname, birthDate);
        setChildSex(childSex);
    }

    public Child(String name, String surname, long birthDate, int iq, Map<String, String> schedule, Sexes childSex) {
        super(name, surname, birthDate, iq, schedule);
        setChildSex(childSex);
    }

    public String getChildSex() {
        return childSex;
    }

    public void setChildSex(Sexes childSex) {
        if(childSex == Sexes.FEMALE) {
            this.childSex = "girl";
        } else if (childSex == Sexes.MALE){
            this.childSex = "boy";
        } else {
            this.childSex = "unknown";
        }
    }

    @Override
    public String prettyFormat(){
        Date humanDOB = new Date(this.getBirthDate());
        LocalDate humanDataOfBirthLocalDate =
                Instant.ofEpochMilli(this.getBirthDate()).atZone(ZoneId.systemDefault()).toLocalDate();
        int yearOfBirth = humanDataOfBirthLocalDate.getYear();
        int monthOfBirth = humanDOB.getMonth()+1;
        int dayOfBirth = humanDataOfBirthLocalDate.getDayOfMonth();
        return this.getChildSex()+": {name='" + this.getName() + '\'' +
                ", surname='" + this.getSurname() + '\'' +
                ", DOB=" +dayOfBirth+"/"+monthOfBirth+"/"+yearOfBirth+
                ", iq=" + this.getIq() +
                ", schedule=" + this.getSchedule() +
                "}\n";
    }
}
