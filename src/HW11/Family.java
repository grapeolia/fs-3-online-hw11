package HW11;


import java.util.*;

public class Family {
    public Woman mother;
    public Man father;
    public List<Child> children;
    public Set<Pet> pets;

    public Family() {
        this.mother = new Woman();
        this.father = new Man();
    }
    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Woman mother, Man father, List<Child> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Woman mother, Man father, List<Child> children, Set<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pets = pets;
    }

    public Woman getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        String motherRecord = mother != null ? mother.toString() : "no name mentioned";
        String fatherRecord = father != null ? father.toString() : "no name mentioned";
        String petRecord = pets != null ? String.valueOf(pets) : "no record";

        return "Family{" +
                "mother=" + motherRecord +
                ", father=" + fatherRecord +
                ", children=" + children +
                ", pet=" + petRecord +
                '}';
    }

    public String prettyFormat(){
        String children = "";
        if(getChildren()!=null && getChildren().size()>0){
            for(Child child:getChildren()){
                children += "\t\t"+child.prettyFormat();
            }
        } else {
            children = "          this family doesn't have any child";
        }
        return "family: "+"\n"+
                "\tmother: "+this.getMother().prettyFormat()+"\n"+
                "\tfather: "+this.getFather().prettyFormat()+"\n"+
                "\tchildren:\n"+children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family family)) return false;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    public void addChild(Child newChild){
        if(getChildren() == null){
            setChildren(List.of(newChild));
        }else{
            if(!getChildren().contains(newChild)) {
                setChildren(new ArrayList<>(getChildren()));
                getChildren().add(newChild);
            }
        }
    }

    public boolean deleteChild(int childIndex){
        if (getChildren() != null
                && getChildren().size()>childIndex
                && getChildren().get(childIndex) != null) {
            setChildren(new ArrayList<>(getChildren()));
            getChildren().remove(childIndex);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human childToDelete) {
        if (getChildren() != null
                && getChildren().size()>0
                && getChildren().contains(childToDelete)) {
            setChildren(new ArrayList<>(getChildren()));
            getChildren().remove(childToDelete);
            return true;
        }
        return false;
    }

    public void addPet(Pet newPet){
        if(pets == null) {
            pets = new HashSet<>();
        }
        pets.add(newPet);
    }

    public boolean deletePet(Pet petToDelete) {
        if (getPets() != null
                && getPets().size()>0
                && getPets().contains(petToDelete)) {
            getPets().remove(petToDelete);
            return true;
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(new StringBuilder().append("You're about to finalize ").append(this).toString());
        super.finalize();
    }

    public int countFamilyMembers(){
            int countPets = getPets() != null
                    ? getPets().size()
                    : 0;
            int countChildren = getChildren() != null
                    ? getChildren().size()
                    : 0;
            return 2+countPets+countChildren;
        }
    }
